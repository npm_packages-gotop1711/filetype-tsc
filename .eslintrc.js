module.exports = {
  root: true,
  env: {
    node: true
  },
  rules: {
    "no-console": process.env.NODE_ENV === "development" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "development" ? "error" : "off"
  }
};
